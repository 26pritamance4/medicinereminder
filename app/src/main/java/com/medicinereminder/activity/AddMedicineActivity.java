package com.medicinereminder.activity;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.medicinereminder.R;
import com.medicinereminder.customviews.NotificationDialogs;
import com.medicinereminder.database.DBHelper;
import com.medicinereminder.global.BaseActivity;
import com.medicinereminder.model.MedicationDataModel;
import com.medicinereminder.utils.AppUtils;
import com.medicinereminder.utils.alarm.Alarm;
import com.medicinereminder.utils.alarm.AlarmReceiver;
import com.medicinereminder.utils.alarm.DateTime;
import com.medicinereminder.utils.global_data_util.IntentKeys;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class AddMedicineActivity extends BaseActivity implements NotificationDialogs.NotificationCallbacks {

    private final String TAG_MEDICINE_DELETE = "tagMedicineDelete";

    /*Initialize Views*/
    TextView tvStartDate, tvTime, tvToolbarSave;
    EditText etMedicineName;
    LinearLayout llWeeklyDay, llStartDate;
    Spinner freqSpinner, pillsSpinner;
    SwitchCompat swAlarm;
    FloatingActionButton fbDeleteMedicine;

    /*Initialize other instance or variables*/
    Context _context;
    int freq = 0, mYear, mMonth, mDay, mHour, mMinute, pillPos = 0, medicineId = 0;
    String frequency = "", pills = "";
    Alarm mAlarm;
    DateTime mDateTime;
    GregorianCalendar mCalendar;
    DBHelper db;
    MedicationDataModel medicationDataModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_medicine);
        _context = this;

        setData();
        checkIntent();
        initView();
        onClickView();
        populateData();
    }

    /*Initialize all objects*/
    private void setData() {
        db = new DBHelper(_context);
        mAlarm = new Alarm(this);
        mDateTime = new DateTime(this);
        mCalendar = new GregorianCalendar();
    }

    /*Get previous activity data from intent*/
    private void checkIntent() {
        if (getIntent().getExtras() != null) {
            medicineId = getIntent().getIntExtra(IntentKeys.INTENT_KEY_ID, 0);
            if (medicineId > 0)
                medicationDataModel = db.getSelectedMedicationData(medicineId);

        }
    }

    /*Define all views*/
    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolbar(toolbar, true, -1);
        if (medicineId > 0)
            setToolbar(toolbar, "Edit Medicine", "Save", 0);
        else
            setToolbar(toolbar, "Add Medicine", "Save", 0);

        tvToolbarSave = (TextView) findViewById(R.id.tvToolbarSave);
        tvStartDate = (TextView) findViewById(R.id.tvStartDate);
        tvTime = (TextView) findViewById(R.id.tvTime);

        etMedicineName = (EditText) findViewById(R.id.etMedicineName);
        swAlarm = (SwitchCompat) findViewById(R.id.swAlarm);
        llWeeklyDay = (LinearLayout) findViewById(R.id.llWeeklyDay);
        llStartDate = (LinearLayout) findViewById(R.id.llStartDate);
        fbDeleteMedicine = (FloatingActionButton) findViewById(R.id.fbDeleteMedicine);

        initializeFreqSpinner();
        initializePillSpinner();
    }

    /*Initialize  medication schedule type spinner*/
    private void initializeFreqSpinner() {
        freqSpinner = (Spinner) findViewById(R.id.freqSpinner);
        ArrayAdapter<CharSequence> freqSelect = ArrayAdapter.createFromResource(_context,
                R.array.freq_select,
                android.R.layout.simple_spinner_item);
        freqSelect.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        freqSpinner.setAdapter(freqSelect);
    }

    /*Initialize quantity of medicine spinner*/
    private void initializePillSpinner() {
        pillsSpinner = (Spinner) findViewById(R.id.spinnerPills);
        ArrayAdapter<CharSequence> pillsSelect = ArrayAdapter.createFromResource(_context,
                R.array.pills_select,
                android.R.layout.simple_spinner_item);
        pillsSelect.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        pillsSpinner.setAdapter(pillsSelect);
    }

    /*Check for new or existing medication*/
    private void populateData() {
        if (medicineId > 0 && medicationDataModel != null) {
            mAlarm.setDate(Long.parseLong(medicationDataModel.startDate));
            editMedication();
        } else {
            addNewMedication();
        }
        populateDateTime();
    }

    /*update data and views for new medication*/
    private void addNewMedication() {
        fbDeleteMedicine.setVisibility(View.GONE);
        swAlarm.setChecked(true);
    }

    /*Update data and views for existing medication*/
    private void editMedication() {
        fbDeleteMedicine.setVisibility(View.VISIBLE);
        if (medicationDataModel.reminderStatus.equals("1"))
            swAlarm.setChecked(true);
        else
            swAlarm.setChecked(false);
        etMedicineName.setText(medicationDataModel.medicineName);

        String[] freqValue = getResources().getStringArray(R.array.freq_select);
        for (int i = 0; i < freqValue.length; i++) {
            if (medicationDataModel.medicineScheduleType.equals(freqValue[i])) {
                freqSpinner.setSelection(i);
            }
        }
        String[] pillsValue = getResources().getStringArray(R.array.pills_select);
        for (int i = 0; i < pillsValue.length; i++) {
            if (medicationDataModel.pills.equals(pillsValue[i])) {
                pillsSpinner.setSelection(i);
            }
        }
    }

    /*Define date and time and update views*/
    private void populateDateTime() {
        mCalendar.setTimeInMillis(mAlarm.getDate());
        mYear = mCalendar.get(Calendar.YEAR);
        mMonth = mCalendar.get(Calendar.MONTH);
        mDay = mCalendar.get(Calendar.DAY_OF_MONTH);
        mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
        mMinute = mCalendar.get(Calendar.MINUTE);

        tvStartDate.setText(mDateTime.formatDate(mAlarm));
        tvTime.setText(mDateTime.formatTime(mAlarm));
    }

    /*Initialize all listener*/
    private void onClickView() {
        fbDeleteMedicine.setOnClickListener(onDeleteListener);
        freqSpinner.setOnItemSelectedListener(spinnerItemSelectedListener);
        pillsSpinner.setOnItemSelectedListener(spinnerItemSelectedListener);
        swAlarm.setOnCheckedChangeListener(swAlarmChangeListener);
        llStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(_context, mDateSetListener, mYear, mMonth, mDay).show();
            }
        });
        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TimePickerDialog(_context, mTimeSetListener, mHour, mMinute, mDateTime.is24hClock()).show();
            }
        });
        tvToolbarSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidate())
                    performNextOps();
            }
        });
    }

    /*Validation check for the data is exist or not in database*/
    private boolean isValidate() {
        if (!etMedicineName.getText().toString().equals("")) {
            if (medicineId > 0) {
                return true;
            } else {
                if (db.checkDataPresentOrNot("" + mAlarm.getDate())) {
                    return true;
                } else {
                    showToast("You already select this time. Please change to another time.");
                }
            }
        } else
            showToast("Medicine name is empty");
        return false;
    }

    /*Delete existing medicine listener*/
    private View.OnClickListener onDeleteListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new NotificationDialogs(AddMedicineActivity.this).showDefaultAlertDialog(_context, "Delete Medicine", "Are you sure you want to delete this medicine?", "", "", TAG_MEDICINE_DELETE);
        }
    };

    /*Listener for turn on off reminder*/
    private SwitchCompat.OnCheckedChangeListener swAlarmChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            mAlarm.setEnabled(isChecked);
        }
    };

    /*Spinner item select listener for medicine schedule type and pill quantity both*/
    private AdapterView.OnItemSelectedListener spinnerItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
            if (parent == freqSpinner) {
                llWeeklyDay.setVisibility(View.GONE);
                freq = position;
                frequency = parent.getItemAtPosition(position).toString();
                switch (position) {
                    case 5:
                        llWeeklyDay.setVisibility(View.VISIBLE);
                        break;
                }
                mAlarm.setOccurence(position);
            } else if (parent == pillsSpinner) {
                pillPos = position;
                pills = parent.getItemAtPosition(position).toString();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    /*Spinner Start Data select listener*/
    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;

            mCalendar = new GregorianCalendar(mYear, mMonth, mDay, mHour, mMinute);
            mAlarm.setDate(mCalendar.getTimeInMillis());
            tvStartDate.setText(mDateTime.formatDate(mAlarm));
        }
    };

    /*Spinner start time select listener*/
    private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            mHour = hourOfDay;
            mMinute = minute;

            mCalendar = new GregorianCalendar(mYear, mMonth, mDay, mHour, mMinute);
            mAlarm.setDate(mCalendar.getTimeInMillis());

            tvTime.setText(mDateTime.formatTime(mAlarm));
        }
    };

    /*After click on save button check reminder on or off and process for that */
    private void performNextOps() {
        mAlarm.setTitle(etMedicineName.getText().toString());
        if (swAlarm.isChecked()) {
            addNotification(mAlarm, "1");
        } else {
            addNotification(mAlarm, "0");
        }
    }

    /*After click on save button input data in data base for Medicine and History table both*/
    private void addNotification(Alarm alarm, String isAlarmchecked) {
        long id = 0;
        if (medicineId > 0) {
            db.updateMedicationData(medicineId, alarm.getTitle(), isAlarmchecked, frequency, "", "" + alarm.getDate(), pills, "1");
        } else {
            id = db.insertMedicationData(alarm.getTitle(), isAlarmchecked, frequency, "", "" + alarm.getDate(), pills, "1");
            medicineId = (int) id;
        }
        db.deleteSelectMedicineHistoryData(medicineId);

        if (isAlarmchecked.equals("1")) {
            int countSize = AppUtils.getCount(frequency);
            for (int i = 0; i < countSize; i++) {
                long interval = AppUtils.setAlarmCount(frequency, alarm.getDate());
                long timeStamp = alarm.getDate() + (interval * i);
                if (timeStamp > System.currentTimeMillis()) {
                    db.insertHistoryData(alarm.getTitle(), (double) timeStamp, pills, "0", medicineId);
                }
            }
        }
        setResult(RESULT_OK);
        finish();
    }

    /*Delete medicine dialog yes button*/
    @Override
    public void onYesPressed(String type) {
        Intent intent = new Intent(_context, AlarmReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(_context, medicineId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager mAlarmManager = (AlarmManager) _context.getSystemService(_context.ALARM_SERVICE);
        mAlarmManager.cancel(sender);
        db.deleteSelectMedicationData(medicineId);
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onNoPressed(String type) {

    }
}
