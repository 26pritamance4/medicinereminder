package com.medicinereminder.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.medicinereminder.R;
import com.medicinereminder.adapter.MedicineAdapter;
import com.medicinereminder.database.DBHelper;
import com.medicinereminder.global.BaseActivity;
import com.medicinereminder.model.MedicationDataModel;
import com.medicinereminder.utils.global_data_util.RequestCode;

import java.util.ArrayList;
import java.util.List;

public class HomeScreenActivity extends BaseActivity {

    /*Initialize views*/
    ImageView ivAnalytics;
    TextView tvEmptyMedication;
    AddFloatingActionButton floatingButton;

    /*Initialize others variables*/
    Context _context;
    MedicineAdapter medicineAdapter;
    List<MedicationDataModel> medicineList = new ArrayList<>();
    DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        _context = this;

        initView();
        onClickView();
        populateData();
    }

    /*define all view*/
    private void initView(){
        db = new DBHelper(_context);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolbar(toolbar, false, -1);
        setToolbar(toolbar, "Home Screen", "", R.drawable.analytics);
        ivAnalytics = (ImageView) findViewById(R.id.ivAnalytics);
        floatingButton = (AddFloatingActionButton) findViewById(R.id.floatingButton);
        tvEmptyMedication = (TextView) findViewById(R.id.tvEmptyMedication);

        initializeRecylerView();
    }

    /*Initialize RecyclerView*/
    private void initializeRecylerView(){
        RecyclerView rvMedicine = (RecyclerView) findViewById(R.id.rvMedicine);
        medicineAdapter = new MedicineAdapter(_context, medicineList);
        GridLayoutManager ooLayoutManagaer
                = new GridLayoutManager(_context, 2);
        rvMedicine.setLayoutManager(ooLayoutManagaer);
        rvMedicine.setHasFixedSize(true);
        rvMedicine.setAdapter(medicineAdapter);
    }

    /*Define all listener*/
    private void onClickView(){
        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(_context, AddMedicineActivity.class);
                startActivityForResult(intent, RequestCode.REQUEST_CODE_ADD_NEW_MEDICINE);
            }
        });
        ivAnalytics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(_context, MedicationAnalyticsActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == RequestCode.REQUEST_CODE_ADD_NEW_MEDICINE){
                populateData();
            }else if(requestCode == RequestCode.REQUEST_CODE_EDIT_MEDICINE){
                populateData();
            }
        }
    }

    /*Refresh MedicineAdapter view*/
    private void populateData(){
        showProgressDialog(_context);
        medicineList.clear();
        medicineList.addAll(db.getMedicationHistory());
        if(medicineList.size() > 0){
            tvEmptyMedication.setVisibility(View.GONE);
            medicineAdapter.notifyDataSetChanged();
        }else{
            tvEmptyMedication.setVisibility(View.VISIBLE);
        }
        hideProgressDialog();
    }
}
