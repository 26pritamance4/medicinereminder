package com.medicinereminder.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.medicinereminder.R;
import com.medicinereminder.adapter.MedicationHistoryAdapter;
import com.medicinereminder.database.DBHelper;
import com.medicinereminder.global.BaseActivity;
import com.medicinereminder.model.MedicationHistoryModel;

import java.util.ArrayList;
import java.util.List;

public class MedicationAnalyticsActivity extends BaseActivity {

    /*Initialize views*/
    TextView tvEmptyHistory;

    /*Initialize others*/
    List<MedicationHistoryModel> historyList = new ArrayList<>();
    MedicationHistoryAdapter medicationHistoryAdapter;
    Context _context;
    DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medication_analytics);

        setData();
        initView();
        populateData();

    }

    /*Initialize objects*/
    private void setData(){
        _context = this;
        db = new DBHelper(_context);
    }

    /*Define all views*/
    private void initView(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolbar(toolbar, true, -1);
        setToolbar(toolbar, "Medication Analytics", "", 0);
        tvEmptyHistory = (TextView) findViewById(R.id.tvEmptyHistory);

        initializeRecylerView();
    }

    /*Initialize RecyclerView*/
    private void initializeRecylerView(){
        RecyclerView rvMedicine = (RecyclerView) findViewById(R.id.rvMedicineAnalytics);
        medicationHistoryAdapter = new MedicationHistoryAdapter(_context, historyList);
        LinearLayoutManager ooLayoutManagaer
                = new LinearLayoutManager(_context);
        rvMedicine.setLayoutManager(ooLayoutManagaer);
        DividerItemDecoration divider = new DividerItemDecoration(rvMedicine.getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(_context, R.drawable.my_custom_divider));
        rvMedicine.addItemDecoration(divider);
        rvMedicine.setHasFixedSize(true);
        rvMedicine.setAdapter(medicationHistoryAdapter);
    }

    /*Refresh MedicationHistoryAdapter view*/
    private void populateData(){
        showProgressDialog(_context);
        historyList.clear();
        historyList.addAll(db.getMedicationAnalytics());
        if(historyList.size() > 0){
            tvEmptyHistory.setVisibility(View.GONE);
            medicationHistoryAdapter.notifyDataSetChanged();
        }else{
            tvEmptyHistory.setVisibility(View.VISIBLE);
        }

        hideProgressDialog();
    }
}
