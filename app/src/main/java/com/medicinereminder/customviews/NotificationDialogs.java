package com.medicinereminder.customviews;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.medicinereminder.R;
/*Making all type of dialog*/
public class NotificationDialogs {

    public NotificationCallbacks notificationCallbacks;

    public NotificationDialogs() {
    }

    public NotificationDialogs(NotificationCallbacks notificationCallbacks) {
        this.notificationCallbacks = notificationCallbacks;
    }

    /*Create default alart dialog*/
    public void showDefaultAlertDialog(Context context, String title,
                                       String message, String okText, String cancelText, final String notificationType) {
        if(okText == null || okText.equals(""))
            okText = context.getResources().getString(R.string.yes_text);
        if(cancelText == null || cancelText.equals(""))
            cancelText = context.getResources().getString(R.string.no_text);
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(okText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with close
                        notificationCallbacks.onYesPressed(notificationType);
                    }
                })
                .setNegativeButton(cancelText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        notificationCallbacks.onNoPressed(notificationType);
                    }
                })
                .show();
    }


    public interface NotificationCallbacks {
        public void onYesPressed(String type);
        public void onNoPressed(String type);
    }
}
