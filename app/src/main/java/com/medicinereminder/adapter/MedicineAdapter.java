package com.medicinereminder.adapter;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.medicinereminder.R;
import com.medicinereminder.activity.AddMedicineActivity;
import com.medicinereminder.model.MedicationDataModel;
import com.medicinereminder.utils.AppUtils;
import com.medicinereminder.utils.alarm.Alarm;
import com.medicinereminder.utils.alarm.AlarmReceiver;
import com.medicinereminder.utils.global_data_util.IntentKeys;
import com.medicinereminder.utils.global_data_util.RequestCode;

import java.util.List;
import java.util.Random;

/**
 * Created by Lenovo on 9/7/2017.
 */

public class MedicineAdapter extends RecyclerView.Adapter<MedicineAdapter.ViewHolder>{

    Context _context;
    List<MedicationDataModel> medicineList;
    Random random;

    public MedicineAdapter(Context context, List<MedicationDataModel> medicineList){
        this._context = context;
        this.medicineList = medicineList;
        random = new Random();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvMedicineName, tvRoutine, tvPillsAmount, tvTime;
        ImageView ivStatus, ivBanner;
        View mainView;

        public ViewHolder(View v) {
            super(v);
            mainView = v;
            setIdsToViews(v);
        }

        private void setIdsToViews(View v) {
            ivBanner = (ImageView) v.findViewById(R.id.ivBanner);
            ivStatus = (ImageView) v.findViewById(R.id.ivStatus);
            tvMedicineName = (TextView) v.findViewById(R.id.tvMedicineName);
            tvRoutine = (TextView) v.findViewById(R.id.tvRoutine);
            tvPillsAmount = (TextView) v.findViewById(R.id.tvPillsAmount);
            tvTime = (TextView) v.findViewById(R.id.tvTime);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView;
        ViewHolder vh;
        rowView = LayoutInflater.from(_context).inflate(R.layout.medicine_item, parent, false);
        vh = new ViewHolder(rowView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MedicationDataModel item = medicineList.get(position);
        performOps(holder, item);
    }

    private void performOps(ViewHolder holder, final MedicationDataModel item){

        String str = "medication_" + random.nextInt(4);

        holder.ivBanner.setImageDrawable(
                        _context.getResources().getDrawable(AppUtils.getResourceID(str, "drawable", _context.getApplicationContext())));

        if(item.reminderStatus.equals("1")){
            if(item.medicineScheduleType.equals("Once a day") && Long.parseLong(item.startDate) > System.currentTimeMillis()){
                holder.ivStatus.setImageResource(R.drawable.green_status);
            }else if(!item.medicineScheduleType.equals("Once a day")){
                long interval = AppUtils.setAlarmCount(item.medicineScheduleType, Long.parseLong(item.startDate));
                if(interval > 0){
                    holder.ivStatus.setImageResource(R.drawable.green_status);
                }else{
                    holder.ivStatus.setImageResource(R.drawable.red_status);
                }
            }else{
                holder.ivStatus.setImageResource(R.drawable.red_status);
            }
        }else{
            holder.ivStatus.setImageResource(R.drawable.black_status);
        }
        holder.tvMedicineName.setText(item.medicineName);
        holder.tvRoutine.setText(item.medicineScheduleType);
        holder.tvPillsAmount.setText("Quantity: " + item.pills);
        holder.tvTime.setText(AppUtils.getStringDateFormatFromUnixTime(item.startDate, "EEE, MMM dd, yyyy hh:mm aa"));

        holder.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(_context, AddMedicineActivity.class);
                intent.putExtra(IntentKeys.INTENT_KEY_ID, item.id);
                ((Activity)_context).startActivityForResult(intent, RequestCode.REQUEST_CODE_EDIT_MEDICINE);
            }
        });

        setAlarm(item);
    }

    private void setAlarm(MedicationDataModel item) {
        PendingIntent sender;
        Intent intent;
        intent = new Intent(_context, AlarmReceiver.class);

        if (item.reminderStatus.equals("1")) {
            Alarm alarm = new Alarm(_context);
            alarm.setId(item.id);
            alarm.setTitle(item.medicineName);
            alarm.setDate(Long.parseLong(item.startDate));
            alarm.setPills(item.pills);
            if(item.reminderStatus.equals("1"))
                alarm.setEnabled(true);
            else
                alarm.setEnabled(false);
            alarm.toIntent(intent);
            sender = PendingIntent.getBroadcast(_context, item.id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager mAlarmManager = (AlarmManager)_context.getSystemService(_context.ALARM_SERVICE);
            if(item.medicineScheduleType.equals("Once a day") && Long.parseLong(item.startDate) > System.currentTimeMillis()){
                mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, Long.parseLong(item.startDate), sender);
            }else if(!item.medicineScheduleType.equals("Once a day")){
                long interval = AppUtils.setAlarmCount(item.medicineScheduleType, Long.parseLong(item.startDate));
                if(interval > 0){
//                    mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, Long.parseLong(item.startDate), sender);
                    mAlarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, Long.parseLong(item.startDate), interval, sender);
                }else{
                    mAlarmManager.cancel(sender);
                }
            }else{
                mAlarmManager.cancel(sender);
            }
        }else{
            sender = PendingIntent.getBroadcast(_context, item.id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager mAlarmManager = (AlarmManager)_context.getSystemService(_context.ALARM_SERVICE);
            mAlarmManager.cancel(sender);
        }
    }

    @Override
    public int getItemCount() {
        return medicineList.size();
    }
}
