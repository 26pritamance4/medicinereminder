package com.medicinereminder.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.medicinereminder.R;
import com.medicinereminder.database.DBHelper;
import com.medicinereminder.model.MedicationHistoryModel;
import com.medicinereminder.utils.AppUtils;

import java.util.List;

/**
 * Created by test on 9/9/2017.
 */

public class MedicationHistoryAdapter extends RecyclerView.Adapter<MedicationHistoryAdapter.ViewHolder>{

    Context _context;
    List<MedicationHistoryModel> historyModelList;

    public MedicationHistoryAdapter(Context context, List<MedicationHistoryModel> historyModelList){
        this._context = context;
        this.historyModelList = historyModelList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView tvStatus, tvMedicineName, tvPillsAmount, tvTime;
        ImageView ivTrash;
        View mainView;

        public ViewHolder(View v) {
            super(v);
            mainView = v;
            setIdsToViews(v);
        }

        private void setIdsToViews(View v){
            tvStatus = (TextView) v.findViewById(R.id.tvStatus);
            tvMedicineName = (TextView) v.findViewById(R.id.tvMedicineName);
            tvPillsAmount = (TextView) v.findViewById(R.id.tvPillsAmount);
            tvTime = (TextView) v.findViewById(R.id.tvTime);
            ivTrash = (ImageView) v.findViewById(R.id.ivTrash);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView;
        ViewHolder vh;
        rowView = LayoutInflater.from(_context).inflate(R.layout.history_item, parent, false);
        vh = new ViewHolder(rowView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MedicationHistoryModel item = historyModelList.get(position);
        performOps(holder, item);
    }

    private void performOps(ViewHolder holder, final MedicationHistoryModel item){
        if(item.takeStatus != null && item.takeStatus.equals("1")){
            holder.tvStatus.setText("Taken");
            holder.tvStatus.setTextColor(ContextCompat.getColor(_context, R.color.appgreenColor));
            holder.tvMedicineName.setTextColor(ContextCompat.getColor(_context, R.color.appgreenColor));
            holder.tvPillsAmount.setTextColor(ContextCompat.getColor(_context, R.color.appgreenColor));
            holder.tvTime.setTextColor(ContextCompat.getColor(_context, R.color.appgreenColor));
        }else{
            if((long)item.timeStamp < System.currentTimeMillis()) {
                holder.tvStatus.setText("Missed");
                holder.tvStatus.setTextColor(ContextCompat.getColor(_context, R.color.redColor));
                holder.tvMedicineName.setTextColor(ContextCompat.getColor(_context, R.color.redColor));
                holder.tvPillsAmount.setTextColor(ContextCompat.getColor(_context, R.color.redColor));
                holder.tvTime.setTextColor(ContextCompat.getColor(_context, R.color.redColor));
            }else {
                holder.tvStatus.setText("In-progress");
                holder.tvStatus.setTextColor(ContextCompat.getColor(_context, R.color.black));
                holder.tvMedicineName.setTextColor(ContextCompat.getColor(_context, R.color.black));
                holder.tvPillsAmount.setTextColor(ContextCompat.getColor(_context, R.color.black));
                holder.tvTime.setTextColor(ContextCompat.getColor(_context, R.color.black));
            }
        }
        holder.tvMedicineName.setText(item.medicineName);
        holder.tvPillsAmount.setText("Quantity: " + item.pills);
        holder.tvTime.setText(AppUtils.getStringDateFormatFromUnixTime(""+item.timeStamp, "EEE, MMM dd, yyyy hh:mm aa"));

        holder.ivTrash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DBHelper(_context).deleteSelectHistry(item.id);
                historyModelList.remove(item);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return historyModelList.size();
    }
}
