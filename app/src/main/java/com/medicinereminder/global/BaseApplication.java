package com.medicinereminder.global;

import android.app.Application;
import android.content.Context;

public class BaseApplication extends Application{

    private static Context _context;

    @Override
    public void onCreate() {
        super.onCreate();
        this._context = this.getApplicationContext();
    }

    public static Context getAppContext() {
        return _context;
    }
}
