package com.medicinereminder.global;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.medicinereminder.R;
import com.medicinereminder.utils.loader_util.LoaderView;

public class BaseActivity extends AppCompatActivity {

    public Dialog customLoaderView;
    public LoaderView loaderView;
    private Animation anim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public boolean showProgressDialog(Context context) {
        customLoaderView = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.toolbar_color));
        }
        customLoaderView.setContentView(R.layout.loader_item);
        LinearLayout llProgressView = (LinearLayout) customLoaderView.findViewById(R.id.llProgressView);
        loaderView = new LoaderView(context);
        llProgressView.addView(loaderView);
        loaderView.setBackgroundColor(Color.TRANSPARENT);

        if (anim == null) {
            createAnimation(llProgressView);
        }
        customLoaderView.show();
        return true;
    }

    protected void createAnimation(LinearLayout view) {
        anim = new RotateAnimation(0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        anim.setRepeatCount(Animation.INFINITE);
        anim.setRepeatMode(Animation.RESTART);
        anim.setDuration(500L);
        view.startAnimation(anim);
    }

    public void hideProgressDialog() {
        if (customLoaderView != null) {
            customLoaderView.dismiss();
            customLoaderView = null;
        }
        if (anim != null)
            anim = null;
    }

    public void showToast(String stringText) {
        Toast.makeText(getBaseContext(), stringText, Toast.LENGTH_SHORT).show();
    }

    public void setToolbar(Toolbar toolbar, String titleText, String saveText, int res) {
        TextView tvToolbarTitle = (TextView) toolbar.findViewById(R.id.tvToolbarTitle);
        TextView tvToolbarSave = (TextView) toolbar.findViewById(R.id.tvToolbarSave);
        ImageView ivNext = (ImageView) toolbar.findViewById(R.id.ivAnalytics);
        if (saveText.equals(""))
            tvToolbarSave.setVisibility(View.GONE);
        if (res > 0) {
            ivNext.setImageResource(res);
            ivNext.setVisibility(View.VISIBLE);
        }
        tvToolbarTitle.setText(titleText);
        tvToolbarSave.setText(saveText);
    }

    public void setToolbar(Toolbar toolbar, boolean isBackEnabled, int navigationIconTintColor) {
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            if (isBackEnabled) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (navigationIconTintColor != -1) {
            if (toolbar != null) {
                if (toolbar.getNavigationIcon() != null)
                    toolbar.getNavigationIcon().setTint(ContextCompat.getColor(BaseApplication.getAppContext(), navigationIconTintColor));
            }
        }
    }
}
