package com.medicinereminder.utils.global_data_util;

/**
 * Created by Lenovo on 9/7/2017.
 */

public class RequestCode {
    public static final int REQUEST_CODE_ADD_NEW_MEDICINE = 10;
    public static final int REQUEST_CODE_EDIT_MEDICINE = 11;
}
