package com.medicinereminder.utils.loader_util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

import com.medicinereminder.R;

public class LoaderView extends View {
    private Animation anim;
    private float x, y;
    private RectF mOval = null;
    private float sweepAngle;

    public LoaderView(Context context) {
        super(context);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        x = getX() + getWidth() / 2;
        y = getY() + getHeight() / 2;
        mOval = new RectF(x - getResources().getDimension(R.dimen.dimen10dp), y - getResources().getDimension(R.dimen.dimen10dp), x + getResources().getDimension(R.dimen.dimen10dp), y + getResources().getDimension(R.dimen.dimen10dp)); //This is the area you want to draw on

        sweepAngle = 90; //Calculate how much of an angle you want to sweep out here

        canvas.drawArc(mOval, 0, sweepAngle, false, getArcPaint(1)); //270 is vertical. I found that starting the arc from just slightly less than vertical makes it look better when the circle is almost complete.

        canvas.drawArc(mOval, 89, sweepAngle, false, getArcPaint(2));

        canvas.drawArc(mOval, 179, sweepAngle, false, getArcPaint(3));

        canvas.drawArc(mOval, 269, sweepAngle, false, getArcPaint(4));
        super.onDraw(canvas);
    }


    private Paint getArcPaint(int code) {
        Paint paint = new Paint();
        paint.setDither(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeJoin(Paint.Join.ROUND);
        switch (code) {
            case 1:
                paint.setColor(getResources().getColor(R.color.loader_blue));
                break;
            case 2:
                paint.setColor(getResources().getColor(R.color.loader_green));
                break;
            case 3:
                paint.setColor(getResources().getColor(R.color.loader_orange));
                break;
            case 4:
                paint.setColor(getResources().getColor(R.color.loader_pink));
                break;
            default:
                break;
        }

        paint.setStrokeWidth(10.0f);
        paint.setAntiAlias(true);
        return paint;
    }


    private void createAnimation(Canvas canvas) {
        anim = new RotateAnimation(0, 359, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setRepeatCount(Animation.INFINITE);
        anim.setRepeatMode(Animation.RESTART);
        anim.setDuration(500L);
        this.startAnimation(anim);
    }
}
