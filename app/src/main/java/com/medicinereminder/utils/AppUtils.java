package com.medicinereminder.utils;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by test on 9/8/2017.
 */

public class AppUtils {

    public static String getStringDateFormatFromUnixTime(String unixTime, String format) {
        if(unixTime != null && !unixTime.equals("0")){
            long intUnix = Long.parseLong(unixTime);
            Date date = new Date(intUnix);
            SimpleDateFormat postFormater = new SimpleDateFormat(format, Locale.getDefault());
            return postFormater.format(date);
        }else {
            Date date = new Date((long) System.currentTimeMillis());
            SimpleDateFormat postFormater = new SimpleDateFormat(format, Locale.getDefault());
            return postFormater.format(date);
        }
    }

    public static long setAlarmCount(String scheduleType , long scheduleTime){
        long intervalTime = 0;
        long currentTime = System.currentTimeMillis();
        switch (scheduleType){
            case "Once a day":
                intervalTime = 0;
                break;
            case "Twice a day":
                intervalTime = 12 * 60 * 60 * 1000;
                if(currentTime > (scheduleTime + intervalTime + 10000))
                    intervalTime = 0;
                break;
            case "Three times a day (Every 8 hours)":
                intervalTime = 8 * 60 * 60 * 1000;
                if(currentTime > (scheduleTime + (intervalTime * 2) + 10000))
                    intervalTime = 0;
                break;
            case "Every 6 hours (Four times a day)":
                intervalTime = 6 * 60 * 60 * 1000;
                if(currentTime > (scheduleTime + (intervalTime * 3) + 10000))
                    intervalTime = 0;
                break;
            case "Every 4 hours (Five times a day)":
                intervalTime = 4 * 60 * 60 * 1000;
                if(currentTime > (scheduleTime + (intervalTime * 4) + 10000))
                    intervalTime = 0;
                break;
            case "Weekly":
                intervalTime = 24 * 60 * 60 * 1000;
                break;
        }
        return intervalTime;
    }

    public static int getCount(String scheduleType){
        int noOfCount = 0;
        switch (scheduleType){
            case "Once a day":
                noOfCount = 1;
                break;
            case "Twice a day":
                noOfCount = 2;
                break;
            case "Three times a day (Every 8 hours)":
                noOfCount = 3;
                break;
            case "Every 6 hours (Four times a day)":
                noOfCount = 4;
                break;
            case "Every 4 hours (Five times a day)":
                noOfCount = 5;
                break;
            case "Weekly":
                noOfCount = 1;
                break;
        }
        return noOfCount;
    }

    public static int getResourceID(String resName, String resType, Context ctx)
    {
        int ResourceID =
                ctx.getResources().getIdentifier(resName, resType,
                        ctx.getApplicationInfo().packageName);
        if (ResourceID == 0)
        {
            throw new IllegalArgumentException
                    (
                            "No resource string found with name " + resName
                    );
        }
        else
        {
            return ResourceID;
        }
    }
}
