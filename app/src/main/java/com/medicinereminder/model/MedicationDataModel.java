package com.medicinereminder.model;

/**
 * Created by Lenovo on 9/7/2017.
 */

public class MedicationDataModel {
    public int id;
    public String medicineName;
    public String reminderStatus;
    public String medicineScheduleType;
    public String weeklyDays;
    public String startDate;
    public String pills;
    public String status;
}
