package com.medicinereminder.model;

/**
 * Created by test on 9/9/2017.
 */

public class MedicationHistoryModel {
    public int id;
    public String medicineName;
    public long timeStamp;
    public String pills;
    public String takeStatus;
    public int medicationId;
}
