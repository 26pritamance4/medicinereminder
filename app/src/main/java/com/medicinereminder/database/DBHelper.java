package com.medicinereminder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.medicinereminder.model.MedicationDataModel;
import com.medicinereminder.model.MedicationHistoryModel;

import java.util.ArrayList;

/**
 * Created by test on 9/7/2017.
 */

public class DBHelper extends SQLiteOpenHelper implements Schema{

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        createMedicationTable(db);
    }

    /*Create medication and history table*/
    public void createMedicationTable(SQLiteDatabase db){
        db.execSQL("CREATE TABLE " + MEDICATION_TABLE_NAME + " (" + MEDICATION_COLUMN_ID +
                " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                MEDICATION_COLUMN_NAME + SQL_TEXT +
                MEDICATION_COLUMN_REMINDER_STATUS + SQL_TEXT +
                MEDICATION_COLUMN_SCHEDULE_TYPE + SQL_TEXT +
                MEDICATION_WEEKLY_DAYS + SQL_TEXT +
                MEDICATION_START_DATE + SQL_TEXT +
                MEDICATION_PILLS_AMOUNT + SQL_TEXT +
                MEDICATION_STATUS + SQL_TEXT_LAST);

        db.execSQL("CREATE TABLE " + HISTORY_TABLE_NAME + " (" + MEDICATION_COLUMN_ID +
                " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                MEDICATION_COLUMN_NAME + SQL_TEXT +
                NOTIFICATION_TIME_STAMP + SQL_DOUBLE +
                MEDICATION_PILLS_AMOUNT + SQL_TEXT +
                MEDICATION_STATUS + SQL_TEXT +
                MEDICATION_ID + SQL_INT +
                "FOREIGN KEY(" + MEDICATION_ID + ") REFERENCES " + MEDICATION_TABLE_NAME +
                "(" + MEDICATION_COLUMN_ID + "))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
    }

    /*Insert data in Medication table*/
    public long insertMedicationData (String medicineName, String reminderStatus, String medicineScheduleType, String weeklyDays, String startDate, String pills, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MEDICATION_COLUMN_NAME, medicineName);
        contentValues.put(MEDICATION_COLUMN_REMINDER_STATUS, reminderStatus);
        contentValues.put(MEDICATION_COLUMN_SCHEDULE_TYPE, medicineScheduleType);
        contentValues.put(MEDICATION_WEEKLY_DAYS, weeklyDays);
        contentValues.put(MEDICATION_START_DATE, startDate);
        contentValues.put(MEDICATION_PILLS_AMOUNT, pills);
        contentValues.put(MEDICATION_STATUS, status);
        long id = db.insert(MEDICATION_TABLE_NAME, null, contentValues);
        return id;
    }

    /*Insert data in History table*/
    public boolean insertHistoryData (String medicineName, double notifyTime, String pills, String takeStatus, int medicationId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MEDICATION_COLUMN_NAME, medicineName);
        contentValues.put(NOTIFICATION_TIME_STAMP, notifyTime);
        contentValues.put(MEDICATION_PILLS_AMOUNT, pills);
        contentValues.put(MEDICATION_STATUS, takeStatus);
        contentValues.put(MEDICATION_ID, medicationId);
        db.insert(HISTORY_TABLE_NAME, null, contentValues);
        return true;
    }

    /*Get selected item from medication table*/
    public MedicationDataModel getSelectedMedicationData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+MEDICATION_TABLE_NAME+" where "+MEDICATION_COLUMN_ID+"="+id+"", null );
        res.moveToFirst();
        MedicationDataModel medicationDataModel = new MedicationDataModel();
        medicationDataModel.id = res.getInt(res.getColumnIndex(MEDICATION_COLUMN_ID));
        medicationDataModel.medicineName = res.getString(res.getColumnIndex(MEDICATION_COLUMN_NAME));
        medicationDataModel.reminderStatus = res.getString(res.getColumnIndex(MEDICATION_COLUMN_REMINDER_STATUS));
        medicationDataModel.medicineScheduleType = res.getString(res.getColumnIndex(MEDICATION_COLUMN_SCHEDULE_TYPE));
        medicationDataModel.weeklyDays = res.getString(res.getColumnIndex(MEDICATION_WEEKLY_DAYS));
        medicationDataModel.startDate = res.getString(res.getColumnIndex(MEDICATION_START_DATE));
        medicationDataModel.pills = res.getString(res.getColumnIndex(MEDICATION_PILLS_AMOUNT));
        medicationDataModel.status = res.getString(res.getColumnIndex(MEDICATION_STATUS));
        res.moveToNext();
        return medicationDataModel;
    }

    /*Validation check before insert data is exist or not*/
    public boolean checkDataPresentOrNot(String timeStamp) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select "+MEDICATION_COLUMN_ID+" from "+MEDICATION_TABLE_NAME+" where "+MEDICATION_START_DATE +"="+timeStamp+"", null );
        if (!(res.moveToFirst()) || res.getCount() ==0){
            return true;
        }
        return false;
    }

    /*Update selected medication data respect of primary key id*/
    public boolean updateMedicationData (Integer id, String medicineName, String reminderStatus, String medicineScheduleType, String weeklyDays, String startDate, String pills, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MEDICATION_COLUMN_NAME, medicineName);
        contentValues.put(MEDICATION_COLUMN_REMINDER_STATUS, reminderStatus);
        contentValues.put(MEDICATION_COLUMN_SCHEDULE_TYPE, medicineScheduleType);
        contentValues.put(MEDICATION_WEEKLY_DAYS, weeklyDays);
        contentValues.put(MEDICATION_START_DATE, startDate);
        contentValues.put(MEDICATION_PILLS_AMOUNT, pills);
        contentValues.put(MEDICATION_STATUS, status);
        db.update(MEDICATION_TABLE_NAME, contentValues, MEDICATION_COLUMN_ID+" = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    /*Update History data respect of foreign key medicationId*/
    public boolean updateHistoryData (Integer medicineId, String takeStatus) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MEDICATION_STATUS, takeStatus);
        db.update(HISTORY_TABLE_NAME, contentValues, MEDICATION_ID+" = ? AND " + NOTIFICATION_TIME_STAMP + " > ? AND " + NOTIFICATION_TIME_STAMP + " < ? ",
                new String[] { Integer.toString(medicineId), Double.toString((double)(System.currentTimeMillis() - 50000)), Double.toString((double) (System.currentTimeMillis() + 10000)) } );
        return true;
    }

    /*Delete medicine from medication table respect of primary key id*/
    public Integer deleteSelectMedicationData (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(MEDICATION_TABLE_NAME,
                MEDICATION_COLUMN_ID+" = ? ",
                new String[] { Integer.toString(id) });
    }

    /*Delete selected medicine all history respect of foreign key medication id*/
    public Integer deleteSelectMedicineHistoryData (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(HISTORY_TABLE_NAME,
                MEDICATION_ID+" = ? AND " + NOTIFICATION_TIME_STAMP+" < ? ",
                new String[] { Integer.toString(id), Double.toString((double) System.currentTimeMillis()) });
    }

    /*Delete selected history respect of primary key id*/
    public Integer deleteSelectHistry (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(HISTORY_TABLE_NAME,
                MEDICATION_COLUMN_ID+" = ? ",
                new String[] { Integer.toString(id) });
    }

    /*Get All medicine data from Medication table*/
    public ArrayList<MedicationDataModel> getMedicationHistory() {
        ArrayList<MedicationDataModel> array_list = new ArrayList<MedicationDataModel>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+MEDICATION_TABLE_NAME, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            MedicationDataModel medicationDataModel = new MedicationDataModel();
            medicationDataModel.id = res.getInt(res.getColumnIndex(MEDICATION_COLUMN_ID));
            medicationDataModel.medicineName = res.getString(res.getColumnIndex(MEDICATION_COLUMN_NAME));
            medicationDataModel.reminderStatus = res.getString(res.getColumnIndex(MEDICATION_COLUMN_REMINDER_STATUS));
            medicationDataModel.medicineScheduleType = res.getString(res.getColumnIndex(MEDICATION_COLUMN_SCHEDULE_TYPE));
            medicationDataModel.weeklyDays = res.getString(res.getColumnIndex(MEDICATION_WEEKLY_DAYS));
            medicationDataModel.startDate = res.getString(res.getColumnIndex(MEDICATION_START_DATE));
            medicationDataModel.pills = res.getString(res.getColumnIndex(MEDICATION_PILLS_AMOUNT));
            medicationDataModel.status = res.getString(res.getColumnIndex(MEDICATION_STATUS));
            array_list.add(medicationDataModel);
            res.moveToNext();
        }
        return array_list;
    }

    /*Get all history from history table.*/
    public ArrayList<MedicationHistoryModel> getMedicationAnalytics() {
        ArrayList<MedicationHistoryModel> array_list = new ArrayList<MedicationHistoryModel>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+HISTORY_TABLE_NAME + " order by " + NOTIFICATION_TIME_STAMP + " desc", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            MedicationHistoryModel historyDataModel = new MedicationHistoryModel();
            historyDataModel.id = res.getInt(res.getColumnIndex(MEDICATION_COLUMN_ID));
            historyDataModel.medicineName = res.getString(res.getColumnIndex(MEDICATION_COLUMN_NAME));
            historyDataModel.timeStamp = (long)res.getDouble(res.getColumnIndex(NOTIFICATION_TIME_STAMP));
            historyDataModel.pills = res.getString(res.getColumnIndex(MEDICATION_PILLS_AMOUNT));
            historyDataModel.takeStatus = res.getString(res.getColumnIndex(MEDICATION_STATUS));
            historyDataModel.medicationId = res.getInt(res.getColumnIndex(MEDICATION_ID));
            array_list.add(historyDataModel);
            res.moveToNext();
        }
        return array_list;
    }
}
