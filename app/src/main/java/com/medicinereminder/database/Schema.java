package com.medicinereminder.database;

/**
 * Created by test on 9/10/2017.
 */

public interface Schema {

    public static final String DATABASE_NAME = "DailyMedication.db";
    public static final String MEDICATION_TABLE_NAME = "medication";
    public static final String MEDICATION_COLUMN_ID = "id";
    public static final String MEDICATION_COLUMN_NAME = "medicineName";
    public static final String MEDICATION_COLUMN_REMINDER_STATUS = "reminderStatus";
    public static final String MEDICATION_COLUMN_SCHEDULE_TYPE = "medicineScheduleType";
    public static final String MEDICATION_WEEKLY_DAYS = "weeklyDays";
    public static final String MEDICATION_START_DATE = "startDate";
    public static final String MEDICATION_PILLS_AMOUNT = "pills";
    public static final String MEDICATION_STATUS = "status";

    public static final String HISTORY_TABLE_NAME = "medicationHistory";
    public static final String NOTIFICATION_TIME_STAMP = "timeStamp";
    public static final String MEDICATION_ID = "medicationId";

    public static final String SQL_INT = " integer, ";
    public static final String SQL_DOUBLE = " double, ";
    public static final String SQL_TEXT = " text, ";
    public static final String SQL_TEXT_LAST = " text)";
}
